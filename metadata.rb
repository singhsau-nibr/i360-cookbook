name       'i360'
maintainer       "Novartis"
maintainer_email "devops@nebula.na.novartis.net"
license          "All rights reserved"
description      "Installs/Configures i360 site"
version          "1.0.8"


#depends "apache2"
depends "mw-apache"
depends "ci"
depends "php"
depends "build-essential"
depends "nibr-oracle-client"
depends "anyapp"
depends 'acme'