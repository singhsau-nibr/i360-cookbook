#
# Cookbook Name:: i360
# Recipe:: default
#
# Copyright 2014, Novartis
#
# All rights reserved - Do Not Redistribute
#

case deploy_env
when 'dev'
  env='dev'
when 'ci'
  env='dev'
when 'prod'
  env='prod'
when 'test'
  env='prod'
else
  env='dev'
end


node.default['apache']['default_modules'] += %w{
  status alias auth_basic authn_file authz_default authz_groupfile authz_host authz_user autoindex
  dir env mime negotiation setenvif rewrite mod_rewrite mod_proxy mod_proxy_http mod_expires mod_headers mod_ssl
}


#include_recipe "apache2"
include_recipe "mw-apache"
include_recipe "ci"
include_recipe "php"
include_recipe "apache2::mod_php5"
include_recipe "build-essential"
include_recipe "nibr-oracle-client"
include_recipe "apache2::mod_proxy"
include_recipe "apache2::mod_proxy_http"

#Set proxy for php pear
execute "set proxy for PEAR" do
  command "pear config-set http_proxy http://#{node['proxy']['url']}"
end

#Commented the below line to deploy the changes
#install php pear


## Installing oci8-2.0.10 package manaually

remote_file "/tmp/oci8-2.0.10.tgz" do
  source "http://cid-archive.nibr.novartis.net/dist/php-oci8/oci8-2.0.10.tgz"
  mode "0666"
  action :create_if_missing
  notifies :run, "bash[oci8-install]", :immediately
end

bash "oci8-install" do
  cwd "/tmp"
  user "root"
  code <<-EOH
  tar -xvzf oci8-2.0.10.tgz
  cd oci8-2.0.10
  phpize
  ./configure --with-oci8=shared,instantclient,/usr/share/oracle/12.1/client64/
  make
  make install
  echo "[OCI8]" >> /etc/php.ini
  echo "extension=oci8.so" >> /etc/php.ini
  EOH
  action :nothing
end

deploy_zipball "i360.zip" do
  release_dir "/var/opt/i360-build-zip"
  app_user node['apache']['user']
  app_group node['apache']['group']
  version node["project_info"]["pinpromote"]["version"]
  project 'i360-build-zip'
  target_dir node['i360']['dir1']
  source  "archive"
end


#deploy RCPOn Call Application
deploy_zipball "rcponcall.zip" do
  release_dir "/var/opt/rcponcall-build-zip"
  app_user node['apache']['user']
  app_group node['apache']['group']
  version node["project_info"]["pinpromote"]["version"]
  project 'rcponcall-build-zip'
  target_dir node['i360']['dir2']
  source  "archive"
end

template "/etc/httpd/sites-available/i360.conf" do
  source "i360.erb"
  variables :port => node["apache"]["listen_ports"].first
end

directory "/var/www/html/apps" do
  recursive true
end

link "/var/www/html/apps/i360" do
  to "/var/www/i360"
  link_type :symbolic
end

#Create a symbolic link for RCPOnCall
link "/var/www/html/apps/rcponcall" do
  to "/var/www/rcponcall"
  link_type :symbolic
end

execute "enable-ssl" do
  command "a2enmod ssl"
  notifies :start, "service[apache2]"
end

execute "a2ensite i360" do
  only_if { File.exists?("/etc/httpd/sites-available/i360.conf") }
end

execute "a2dissite default" do
  only_if { File.exists?("/etc/httpd/sites-available/default.conf") }
end

link "/var/www/html/apps/i360/dist/php/getIssueTypeMetadata.php" do
 to "/var/www/html/apps/i360/dist/php/getIssueTypeMetadata-"+env+".php"
 link_type :symbolic
 action :create
end

link "/var/www/html/apps/i360/dist/php/JIRAAuthenticator.php" do
 to "/var/www/html/apps/i360/dist/php/JIRAAuthenticator-"+env+".php"
 link_type :symbolic
 action :create
end

link "/var/www/html/apps/i360/dist/php/getBlueBookIssues.php" do
 to "/var/www/html/apps/i360/dist/php/getBlueBookIssues-"+env+".php"
 link_type :symbolic
 action :create
end

link "/var/www/html/apps/i360/dist/php/getJIRAProjects.php" do
 to "/var/www/html/apps/i360/dist/php/getJIRAProjects-"+env+".php"
 link_type :symbolic
 action :create
end

link "/var/www/html/apps/i360/dist/php/getServerProvisioningIssues.php" do
 to "/var/www/html/apps/i360/dist/php/getServerProvisioningIssues-"+env+".php"
 link_type :symbolic
 action :create
end

link "/var/www/html/apps/i360/dist/php/getServerRetirementStorageIssues.php" do
 to "/var/www/html/apps/i360/dist/php/getServerRetirementStorageIssues-"+env+".php"
 link_type :symbolic
 action :create
end

execute "chown-apache" do
  command "chown -R #{node['i360']['user']}:#{node['i360']['group']} #{node['i360']['dir1']}"  
end

execute "chmod-apache" do
  command "chmod -R 750 #{node['i360']['dir1']}"
  notifies :start, "service[apache2]"
end

# This will update the credentials file required by application for accessing JIRA
template "/var/www/html/apps/i360/dist/php/JIRACredentials.php" do
        source "JIRA_Credentials.php.erb"
        mode 0750
        user node['i360']['user']
        group node['i360']['group']
        variables(
          :username  => data_bag_item("projects", "i360")["username"],
          :password => data_bag_item("projects", "i360")["password"]
          )
  action :create        
end

template "/var/www/html/apps/i360/dist/php/cacert.pem" do
  source 'cacert.pem.erb'
  mode 0750
  user node['i360']['user']
  group node['i360']['group']
  action :create_if_missing
end

case deploy_env
when 'dev'
include_recipe 'acme'

# Set up contact information. Note the mailto: notation
node.set['acme']['contact'] = [ 'sanjay-1.singh@novartis.com' ] 
# Real certificates please...
node.set['acme']['dir'] = 'https://nrusca-slt4047.nibr.novartis.net:8443/ACME-server/directory' 


site = node.fqdn
sans = [node.fqdn]

acme_certificate "#{site}" do
    crt      "/var/certs/#{site}.crt"
    key      "/var/certs/#{site}.key"
    chain    "/var/certs/#{site}.pem"
    notifies :restart, "service[apache2]"
    wwwroot  "/var/www/html/apps/i360"
    alt_names sans
end 

end